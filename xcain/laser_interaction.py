import numpy as np
import tempfile
from pathlib import Path
import subprocess
import pandas as pd
import math
from schema import Schema, Use, And, Optional

to_float = lambda x: float(x)

LASER_PARAMETER_SCHEMA = Schema(
    {
        Optional('angle_deg', default=0.):  Use(to_float,  error='Invalid angle.'),
        'pulseE':                           Use(to_float,  error='Invalid pulse energy.'),
        'sigLrx':                           Use(to_float,  error='Invalid laser spot size x.'),
        'sigLry':                           Use(to_float,  error='Invalid laser spot size y.'),
        'laserwl':                          Use(to_float,  error='Invalid laser wave length.'),
        'sigt':                             Use(to_float,  error='Invalid pulse length.'),
        'shifting_laser_x' :                Use(to_float,  error='Invalid laser IP shift x.'),
        'shifting_laser_y' :                Use(to_float,  error='Invalid laser IP shift y.'),
        'shifting_laser_s' :                Use(to_float,  error='Invalid laser IP shift s.'),
        'shifting_laser_t' :                Use(to_float,  error='Invalid laser IP shift t.'),
        'NPH':                              And(int, lambda x: x >= 0,      error='Invalid max. number of photons absorbed.'),
        'N_t_steps':                        And(int, lambda x: x > 0,       error='Invalid number of time steps.'),
        'STOKES_1':                         And(lambda x: x in (-1, 0, 1),  error='Invalid stoke parameter 1.'),
        'STOKES_2':                         And(lambda x: x in (-1, 0, 1),  error='Invalid stoke parameter 2.'),
        'STOKES_3':                         And(lambda x: x in (-1, 0, 1),  error='Invalid stoke parameter 3.'),
    }, ignore_extra_keys=True
)

CAIN_COLUMNS = [
    'K',
    'GEN',
    'Weight',
    'T',
    'X',
    'Y',
    'Z',
    'Etot',
    'Px',
    'Py',
    'Pz',
    'Sx',
    'Sy',
    'Ss',
]

CAIN_INPUT_FILENAME = 'cain_input.i'
CAIN_BEAM_FILENAME = 'beam.txt'
CAIN_LOG_FILENAME = 'cain_log.txt'
CAIN_OUTPUT_FILENAME = 'cain_output_electrons.dat'
CAIN_PHOTON_OUTPUT_FILENAME = 'cain_output_photons.dat'


def translate_to_cain(particles, particle_range, working_directory:Path) -> None:
    cain_df = pd.DataFrame(columns=CAIN_COLUMNS)

    cain_df['K'] = (np.ones(len(particles.x[particle_range[0]:particle_range[1]]))*2).astype(int)
    cain_df['GEN'] = (np.ones(len(particles.x[particle_range[0]:particle_range[1]]))*1).astype(int)
    cain_df['Weight'] = np.ones(len(particles.x[particle_range[0]:particle_range[1]]))*particles.weight[particle_range[0]:particle_range[1]]

    cain_df['T'] = np.zeros(len(particles.x[particle_range[0]:particle_range[1]]))
    cain_df['X'] = particles.x[particle_range[0]:particle_range[1]]
    cain_df['Y'] = particles.y[particle_range[0]:particle_range[1]]
    cain_df['Z'] = particles.zeta[particle_range[0]:particle_range[1]]

    ptot = particles.p0c[particle_range[0]:particle_range[1]]*(1+particles.delta[particle_range[0]:particle_range[1]])
    cain_df['Etot'] = particles.p0c[particle_range[0]:particle_range[1]]*particles.ptau[particle_range[0]:particle_range[1]] + particles.energy0[particle_range[0]:particle_range[1]]
    cain_df['Px'] = particles.px[particle_range[0]:particle_range[1]]*particles.p0c[particle_range[0]:particle_range[1]]
    cain_df['Py'] = particles.py[particle_range[0]:particle_range[1]]*particles.p0c[particle_range[0]:particle_range[1]]
    cain_df['Pz'] = np.sqrt(ptot**2 - cain_df['Px']**2 - cain_df['Py']**2)

    cain_df['Sx'] = np.zeros(len(particles.x[particle_range[0]:particle_range[1]]))
    cain_df['Sy'] = np.zeros(len(particles.x[particle_range[0]:particle_range[1]]))
    cain_df['Ss'] = np.zeros(len(particles.x[particle_range[0]:particle_range[1]]))

    # TODO: convert to pd.to_csv, ensuring correct formatting
    np.savetxt( Path(working_directory)/CAIN_BEAM_FILENAME,
                cain_df.to_numpy(),
                fmt=' %i    %i       %1.12e % 1.12e % 1.12e % 1.12e % 1.12e % 1.12e % 1.12e % 1.12e % 1.12e % 1.12e % 1.12e % 1.12e ')


def run_cain(cain_path: Path, working_directory: Path) -> None:
    subprocess.run([str(cain_path)],
                   input=open(working_directory/CAIN_INPUT_FILENAME, 'r', encoding='utf-8').read(), # instead of going via file, use str directly
                   cwd=working_directory,
                   check=True,
                   text=True,
                   stdout=open(working_directory/CAIN_LOG_FILENAME, 'w', encoding='utf-8'),
                   stderr=subprocess.PIPE,
                   encoding='utf-8')


def prepare_cain_input(cain_parameter: dict, particles, particle_range, seed: int, working_directory: Path) -> None:
    angle=cain_parameter['angle_deg']*(math.pi/180)

    with open(Path(working_directory)/CAIN_INPUT_FILENAME, 'w', encoding='utf-8') as cain_input:
        cain_input.write(f" ALLOCATE  MP={3*len(particles.x[particle_range[0]:particle_range[1]]):d};\n")
        cain_input.write(" \n")
        cain_input.write(" SET   photon=1, electron=2, positron=3, mm=1D-3, micron=1D-6, nm=1D-9, mu0=4*Pi*1D-7, \n")
        cain_input.write("  psec=1e-12*Cvel,\n")
        cain_input.write(" \n")
        cain_input.write(f"sigz={np.std(particles.zeta[particle_range[0]:particle_range[1]]):f},\n")
        cain_input.write("ntcut=5,\n")
        cain_input.write(f"laserwl={cain_parameter['laserwl']:f}*nm, \n")
        cain_input.write(f"pulseE={cain_parameter['pulseE']:f},\n")
        cain_input.write(f"sigLrx={cain_parameter['sigLrx']:f}*micron,\n")
        cain_input.write(f"sigLry={cain_parameter['sigLry']:f}*micron,\n")
        cain_input.write("w0x=2*sigLrx,\n")
        cain_input.write("w0y=2*sigLry,\n")
        cain_input.write("raylx=Pi*w0x^2/laserwl, \n")
        cain_input.write("rayly=Pi*w0y^2/laserwl, \n")
        cain_input.write(f"sigt={cain_parameter['sigt']:f}*psec,\n")
        cain_input.write(f"angle={angle:f},  \n")
        cain_input.write("tdl=1.0,\n")
        cain_input.write(" powerd=(2*pulseE*Cvel)/[Pi*sigt*Sqrt(2*Pi)*w0x*w0y],\n")

        if cain_parameter['NPH']>=1:
            cain_input.write(" xi=(laserwl/(2*Pi*Emass))*Sqrt(powerd*mu0*Cvel),\n")
            cain_input.write(f" ee= {particles.energy0[0]:f}, !electron energy\n")
            cain_input.write(" lambda=(8*Pi*ee*Hbarc)/(laserwl*Emass^2),\n")

        cain_input.write(" \n")
        cain_input.write(" SET MsgLevel=1;\n")
        cain_input.write(" \n")
        cain_input.write(f" SET Rand=5*{seed:f};\n")
        cain_input.write(" \n")
        cain_input.write(f" BEAM FILE='{CAIN_BEAM_FILENAME}';\n")
        cain_input.write(" \n")
        cain_input.write(" LASER LEFT, WAVEL=laserwl, POWERD=powerd,\n")
        cain_input.write(" \n")
        cain_input.write(f"       TXYS=({cain_parameter['shifting_laser_t']:f}, {cain_parameter['shifting_laser_x']:f}, {cain_parameter['shifting_laser_y']:f}, {cain_parameter['shifting_laser_s']:f}), \n")
        cain_input.write("       E3=(-Sin(angle),0.0,-Cos(angle)), E1=(0,1,0),\n")
        cain_input.write(f"       RAYLEIGH=(raylx,rayly), SIGT=sigt, GCUTT=ntcut, STOKES=({cain_parameter['STOKES_1']:f}, {cain_parameter['STOKES_2']:f}, {cain_parameter['STOKES_3']:f}),\n")
        cain_input.write("       TDL=(tdl,tdl) ;\n")
        cain_input.write(" \n")
        cain_input.write(" \n")
        if cain_parameter['NPH']==0:
            cain_input.write(" LASERQED  COMPTON, NPH=0;\n")
        else:
            cain_input.write(f" LASERQED  COMPTON, NPH={cain_parameter['NPH']:2.0f}, XIMAX=1.1*xi, LAMBDAMAX=5.1*lambda, PMAX=10 ;\n")

        cain_input.write(" SET MsgLevel=0;  FLAG OFF ECHO;\n")
        cain_input.write(" SET Smesh=sigt/3;\n")
        # cain_input.write(" SET emax=1.001*ee, wmax=emax;\n");
        cain_input.write(" SET  it=0;\n")
        cain_input.write(" \n")
        cain_input.write(f" PUSH  Time=(-ntcut*(sigt+sigz),ntcut*(sigt+sigz),{cain_parameter['N_t_steps']:f});\n")
        cain_input.write("       IF Mod(it,20)=0;\n")
        cain_input.write("        PRINT it, FORMAT=(F6.0,'-th time step'); PRINT STAT, SHORT;\n")
        cain_input.write("       ENDIF;\n")
        cain_input.write("      SET it=it+1;\n")
        cain_input.write(" ENDPUSH;\n")
        cain_input.write(" \n")
        cain_input.write("DRIFT T=0;\n") # beam not take into account legth of interaction (like thin element L=0) Important when we use it different traking code to not change orbit length
        cain_input.write(" \n")
        cain_input.write(f"WRITE BEAM, KIND=(electron), FILE='{CAIN_OUTPUT_FILENAME}';\n")
        cain_input.write(" \n")
        cain_input.write("!PRINT STATISTICS, FILE='ELECRON_STAT.DAT';\n")
        cain_input.write(" \n")
        cain_input.write(f"WRITE BEAM, KIND=(photon), FILE='{CAIN_PHOTON_OUTPUT_FILENAME}';\n")
        cain_input.write(" \n")
        cain_input.write(" \n")


def load_cain_files(particles, particle_range, working_directory, photon_file):

    cain_df = pd.read_csv(
        Path(working_directory)/CAIN_OUTPUT_FILENAME,
        sep=r'\s+',
        skiprows=1,
        names=CAIN_COLUMNS,
        converters={ col: lambda x: float(x.replace('D', 'E')) for col in CAIN_COLUMNS
        })
    particles.x[particle_range[0]:particle_range[1]] = cain_df['X']
    particles.y[particle_range[0]:particle_range[1]] = cain_df['Y']
    particles.zeta[particle_range[0]:particle_range[1]] = cain_df['Z']

    particles.px[particle_range[0]:particle_range[1]] = cain_df['Px']/particles.p0c[particle_range[0]:particle_range[1]]
    particles.py[particle_range[0]:particle_range[1]] = cain_df['Py']/particles.p0c[particle_range[0]:particle_range[1]]
    particles.delta[particle_range[0]:particle_range[1]] = np.sqrt(cain_df['Pz']**2  + cain_df['Px']**2 + cain_df['Py']**2)/particles.p0c[particle_range[0]:particle_range[1]] - 1

    if photon_file is not None:
        photon_df = pd.read_csv(
            Path(working_directory)/CAIN_PHOTON_OUTPUT_FILENAME,
            sep=r'\s+',
            skiprows=1,
            names=CAIN_COLUMNS,
            converters={ col: lambda x: float(x.replace('D', 'E')) for col in CAIN_COLUMNS
            })
            
        turn = np.unique(particles.at_turn[particle_range[0]:particle_range[1]])
        assert len(turn) == 1, f"{turn}, {particles.at_turn[particle_range[0]:particle_range[1]]}"
        photon_df['turn'] = turn[-1]
        photons_df = pd.read_hdf(photon_file, key='photons', mode='a')
        photons_df = pd.concat([photons_df, photon_df], ignore_index=True)
        photons_df.to_hdf(photon_file, key='photons', mode='w')


def get_active_particle_id_range(particles):
    """
    Get the range of particle indices of active particles.
    """
    ctx2np = particles._buffer.context.nparray_from_context_array
    idx_active = np.argwhere(ctx2np(particles.state) > 0)
    return idx_active[0][0], idx_active[-1][0] + 1


class LaserInteraction():
    def __init__(self, name, cain_path: Path, laser_parameter: dict, seed: int, photon_file: Path):
        self.cain_path = Path(cain_path).absolute()
        self.laser_parameter = LASER_PARAMETER_SCHEMA.validate(laser_parameter)
        self.seed = seed
        self.is_on = True
        self.photon_file = photon_file
        if self.photon_file is not None:
            (pd.DataFrame()).to_hdf(self.photon_file, key='photons', mode='w')


    def track(self, particles):
        if not self.is_on:
            return
        with tempfile.TemporaryDirectory() as temp_dir:
            temp_dir = Path(temp_dir)

            particles.reorganize()
            particle_range = get_active_particle_id_range(particles)

            assert (particle_range[1] - particle_range[0]) > 1000, "Number of particles too low."
            translate_to_cain(particles, particle_range, temp_dir)
            prepare_cain_input(self.laser_parameter, particles, particle_range, self.seed, temp_dir)
            run_cain(self.cain_path, temp_dir)
            load_cain_files(particles, particle_range, temp_dir, self.photon_file)
        self.seed = self.seed + 1


CUSTOM_ELEMENTS = {'LaserInteraction': LaserInteraction}
