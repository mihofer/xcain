import pytest
from pathlib import Path
import numpy as np
import pandas as pd
import xobjects as xo
import xpart as xp

from xcain import laser_interaction as xc
from schema import SchemaError

INPUT_DIR = Path(__file__).parent / "input"
CAIN_EXECUTABLE = Path(__file__).parent.parent.parent / "cain_executable" / "cain_compiled.gcc"
XSUITE_COORDINATES = ['x', 'px', 'y', 'py', 'zeta', 'delta']


EXAMPLE_LASER_PARAMETER = {
        'angle_deg':0,               # in degree
        'pulseE':1.00,           #laser puse energy [J]
        'sigLrx':100,                 #/2; % given in [mu m] micro meter like 2 weist w0=28;
        'sigLry':100,                 #/2; % given in [mu m] micro meter like 2 weist w0=28;
        'laserwl':800,                # laser wavelenth [nm] nano meters
        'sigt':300,                    #pulse length [ps]
        'shifting_laser_x' : 0,  #
        'shifting_laser_y' : 0,  #
        'shifting_laser_s' : 0,  #
        'shifting_laser_t' : 0,  #shifting_laser_t;  %

        'NPH':0, #is Maximum number of laser photons to be absorbed in one process NPH=0 linear, NPH>= 1, use nonlinear formula.

        'N_t_steps':250, #Number of time steps for linear can be 250-300 for non-linear bigger

        #STOKES:  linear [0 0 1]; circular [0 1 0]
        'STOKES_1':0,
        'STOKES_2':0,
        'STOKES_3':1,
    }


# test that unit conversion is internally consistent
def test_conversion(_xparticles, tmp_path):
    particles = _xparticles.copy()
    active_range = particles.get_active_particle_id_range()
    xc.translate_to_cain(particles, active_range, tmp_path)

    # open input beam file, add line to top of file and write as cain input file
    with open(tmp_path/xc.CAIN_OUTPUT_FILENAME, 'w') as output_file, open(tmp_path/xc.CAIN_BEAM_FILENAME, 'r') as input_file :
        output_file.write(' '.join(xc.CAIN_COLUMNS))
        output_file.write('\n')
        output_file.write(input_file.read())

    xc.load_cain_files(particles, active_range, tmp_path, None)
    particles = particles.to_pandas()
    _xparticles = _xparticles.to_pandas()

    pd.testing.assert_frame_equal(
        particles.loc[:, XSUITE_COORDINATES],
        _xparticles.loc[:, XSUITE_COORDINATES]
        )


# test that cain runs and provides output file
def test_run_cain(_xparticles, tmp_path):
    particles = _xparticles.copy()
    active_range = particles.get_active_particle_id_range()
    _xparticles = _xparticles.to_pandas()
    xc.translate_to_cain(particles, active_range, tmp_path)

    xc.prepare_cain_input(EXAMPLE_LASER_PARAMETER, particles, active_range, 1, tmp_path)
    assert (tmp_path/xc.CAIN_INPUT_FILENAME).exists()

    xc.run_cain(CAIN_EXECUTABLE, tmp_path)
    assert (tmp_path/xc.CAIN_BEAM_FILENAME).exists()

    (pd.DataFrame()).to_hdf(tmp_path/'photon_file.hdf5', key='photons', mode='w')
    xc.load_cain_files(particles, active_range, tmp_path, tmp_path/'photon_file.hdf5')
    particles = particles.to_pandas()

    with pytest.raises(AssertionError):
        pd.testing.assert_frame_equal(
            particles.loc[:, XSUITE_COORDINATES],
            _xparticles.loc[:, XSUITE_COORDINATES]
            )

# test that cain runs and provides output file
def test_check_too_few(tmp_path):
    
    emitt_x =  0.71e-9
    emitt_y = 1.42e-12
    beta_x = 100.
    beta_y = 100.
    sigma_z = 2.8*10**-3
    n_part = int(5e2)
    bunch_intensity = 2.43e11
    context = xo.ContextCpu()

    particles = xp.Particles(
        _context=context,
        mass0=xp.ELECTRON_MASS_EV,
        q0=1,
        p0c=45.6e9,
        weight=bunch_intensity/n_part,
        x=np.random.normal(loc=0., scale=np.sqrt(emitt_x*beta_x), size=n_part),
        px=np.random.normal(loc=0., scale=np.sqrt(emitt_x/beta_x), size=n_part),
        y=np.random.normal(loc=0., scale=np.sqrt(emitt_y*beta_y), size=n_part),
        py=np.random.normal(loc=0., scale=np.sqrt(emitt_y/beta_y), size=n_part),
        zeta=np.random.normal(loc=0., scale=sigma_z, size=n_part),
        delta=np.random.normal(loc=0., scale=0.01, size=n_part),
    )
    
    xcli = xc.LaserInteraction(
        'laser',
        CAIN_EXECUTABLE,
        EXAMPLE_LASER_PARAMETER,
        1,
        None
        )

    with pytest.raises(AssertionError):
        xcli.track(particles)


def test_laser_parameter():
    xc.LASER_PARAMETER_SCHEMA.validate(EXAMPLE_LASER_PARAMETER)
    failing_dict = EXAMPLE_LASER_PARAMETER.copy()
    failing_dict.pop('angle_deg')
    xc.LASER_PARAMETER_SCHEMA.validate(failing_dict)
    failing_dict.pop('pulseE')
    with pytest.raises(SchemaError):
        xc.LASER_PARAMETER_SCHEMA.validate(failing_dict)


@pytest.fixture(scope="module")
def _xparticles():

    emitt_x =  0.71e-9
    emitt_y = 1.42e-12
    beta_x = 100.
    beta_y = 100.
    sigma_z = 2.8*10**-3
    n_part = int(1e4)
    bunch_intensity = 2.43e11
    context = xo.ContextCpu()

    particles = xp.Particles(
        _context=context,
        mass0=xp.ELECTRON_MASS_EV,
        q0=1,
        p0c=45.6e9,
        weight=bunch_intensity/n_part,
        x=np.random.normal(loc=0., scale=np.sqrt(emitt_x*beta_x), size=n_part),
        px=np.random.normal(loc=0., scale=np.sqrt(emitt_x/beta_x), size=n_part),
        y=np.random.normal(loc=0., scale=np.sqrt(emitt_y*beta_y), size=n_part),
        py=np.random.normal(loc=0., scale=np.sqrt(emitt_y/beta_y), size=n_part),
        zeta=np.random.normal(loc=0., scale=sigma_z, size=n_part),
        delta=np.random.normal(loc=0., scale=0.01, size=n_part),
    )

    return particles
