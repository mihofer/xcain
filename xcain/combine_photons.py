from multiprocessing import Pool
from pathlib import Path
import pandas as pd
import tfs
from generic_parser import EntryPointParameters, entrypoint


def get_params():
    params = EntryPointParameters()
    params.add_parameter(
        name="job_file",
        type=str,
        required=True,
        help="Path to the tfs file.",
    )
    params.add_parameter(
        name="outputfolder",
        type=str,
        default='Outputdata',
        help="Name of the outputfolder.",
    )
    params.add_parameter(
        name="photons_file",
        type=str,
        default='photons.hdf',
        help="Name of the photons file.",
    )    
    params.add_parameter(
        name="merged_photons_file",
        type=str,
        default=None,
        help="Name of the merged photons file.",
    )
    
    return params


def _read_photons_hdf(filename):
    return pd.read_hdf(filename, key='photons')

@entrypoint(get_params(), strict=True)
def merge_photons(opt):

    job_df = tfs.read(opt.job_file, index='JobId')

    job_df['JobOutputFile'] = job_df['JobDirectory'].apply(lambda x: str(Path(x)/opt.outputfolder/opt.photons_file))

    p = Pool()
    photon_dataframes = p.map(_read_photons_hdf, job_df['JobOutputFile'].tolist())
    photon_df = pd.concat(photon_dataframes, ignore_index=True)

    if opt.merged_photons_file is not None:
        photon_df.to_csv(opt.merged_photons_file, index_label='ParticleID')

    return photon_df


if __name__ == "__main__":
    merge_photons()
