# XCain

XCain provides an interface between [Xsuite](https://github.com/xsuite) particle tracking code and the [Cain](https://www-jlc.kek.jp/subg/ir/lib/cain21b.manual/node2.html) Monte-Carlo e+/e-/gamma interaction code.

## Getting started

Translation and interface are based on the [scripts](https://github.com/DrIlrrr/PY_CAIN_SHELL_for_XSUIT) by I. Drebot.

To run, first install the required python packages by running 

`python -m pip install .`.

Note that as of time of writing, only Python 3.9 is supported.

For using, install a `LaserInteraction` element in the beamline following

```python
from xcain import laser_interaction as xc

laser_ip = xc.LaserInteraction('LaserIP',
                                PATH_TO_CAIN_EXECUTABLE,
                                DICTIONARY_OF_LASERPARAMETER,
                                seed=1,
                                photon_file=None)

```
